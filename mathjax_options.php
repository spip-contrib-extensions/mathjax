<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Initialiser la globales si jamais pas encore fait
$GLOBALS['spip_wheels'] = $GLOBALS['spip_wheels'] ?? [];
$GLOBALS['spip_wheels']['raccourcis'] = array_merge(
	['mathjax/mathjax'],
	$GLOBALS['spip_wheels']['raccourcis'] ?? []
);

