<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/mathjax.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mathjax_description' => 'Le plugin remplace les balises <code><math></code> et <code></math></code> de SPIP.',
	'mathjax_nom' => 'MathJax pour SPIP !',
	'mathjax_slogan' => 'Afficher des équations et des fonctions mathématiques'
);
