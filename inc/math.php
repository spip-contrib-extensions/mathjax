<?php

/***************************************************************************\
 *  SPIP, Système de publication pour l'internet                           *
 *                                                                         *
 *  Copyright © avec tendresse depuis 2001                                 *
 *  Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribué sous licence GNU/GPL.     *
 *  Pour plus de détails voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

//
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Fonction appelee par propre() s'il repere un mode <math>
// https://code.spip.net/@traiter_math
function traiter_math($letexte, $source = '', $defaire_amp = false, $accepter_formules_block = true) {

	if ($collection = mathjax_collecter_balises_math($letexte)) {
		$collection = array_reverse($collection);


		foreach ($collection as $c) {
			$texte_milieu = math_traiter_texte($c['match'][1], $source, $defaire_amp, $accepter_formules_block);
			$letexte = substr_replace($letexte, $texte_milieu, $c['pos'], $c['length']);
		}
	}

	return $letexte;
}

/**
 * Collecter les balises math
 * @param string $letexte
 * @return array
 */
function mathjax_collecter_balises_math($letexte) {
	$preg = '@<math>(.*)(</math>|$)@iUsS';
	if (class_exists(Spip\Texte\Collecteur\CollecteurHtmlTag::class)) {
		$collecteur_math = new Spip\Texte\Collecteur\CollecteurHtmlTag('math', $preg, '');
		$collection = $collecteur_math->collecter($letexte);
	} else {
		$pos = 0;
		$collection = [];
		while (
			(($next = strpos($letexte, '<math', $pos)) !== false)
			&& preg_match($preg, $letexte, $r, PREG_OFFSET_CAPTURE, $next)
		) {
			$found_pos = $r[0][1];
			$found_length = strlen($r[0][0]);
			$match = [
				'raw' => $r[0][0],
				'match' => array_column($r, 0),
				'pos' => $found_pos,
				'length' => $found_length
			];

			$collection[] = $match;

			$pos = $match['pos'] + $match['length'];
		}
	}

	return $collection;
}

/**
 * Traiter le contenu texte d'une balise math
 * @param $texte_milieu
 * @param $source
 * @param $defaire_amp
 * @param $accepter_formules_block
 * @return array|mixed|string|string[]
 */
function math_traiter_texte($texte_milieu, $source = '', $defaire_amp = false, $accepter_formules_block = true) {
	$traitements = [
		// Les doubles $$x^2$$ en mode 'div'
		['str' => '$$', 'preg' => ',[$][$]([^$]+)[$][$],', 'pre' => "\n<p class=\"spip spip-math\">$$", 'post' => "$$</p>\n", 'isblock' => true],
		// Les simples $x^2$ en mode 'span'
		['str' => '$', 'preg' => ',[$]([^$]+)[$],', 'pre' => '<span class="spip-math">$', 'post' => '$</span>', 'isblock' => false],
	];

	foreach ($traitements as $t) {
		while (
			strpos($texte_milieu, $t['str']) !== false
			&& preg_match($t['preg'], $texte_milieu, $regs)
		) {
			$expression = $regs[1];
			if ($t['isblock'] && !$accepter_formules_block) {
				$echap = code_echappement(str_replace('$', '&#36;', $regs[0]), $source);
			}
			else {
				if ($defaire_amp) {
					$expression = str_replace('&amp;', '&', $expression);
				}
				$echap = $t['pre'] . $expression . $t['post'];
				if (is_callable($GLOBALS['math_rendering_callback'] ?? '')) {
					$echap = call_user_func($GLOBALS['math_rendering_callback'], $echap);
				}
				$echap = code_echappement($echap, $source);
			}
			$pos = strpos($texte_milieu, (string) $regs[0]);
			$texte_milieu = substr_replace($texte_milieu, $echap, $pos, strlen($regs[0]));
		}
	}
	return $texte_milieu;
}

/**
 * Propager les <math>..</math> à l'intérieur des notes de bas de page qu'elles contiennent éventuellement
 * car ces notes seront traitées séparemment
 *
 * @param string $texte
 * @return string
 */
function math_propager_dans_les_notes($texte) {

	$pos_courante = 0;
	while (
		($pos_ouvrante = strpos($texte, '[[', $pos_courante)) !== false
		&& ($pos_fermante = strpos($texte, ']]', $pos_ouvrante)) !== false
	) {
		$pdollar = strpos($texte, '$', $pos_ouvrante);
		if ($pdollar !== false && $pdollar < $pos_fermante) {
			$texte = substr_replace($texte, '</math>[[<math>', $pos_ouvrante, 2);
			$pos_fermante += 13;
			$texte = substr_replace($texte, '</math>]]<math>', $pos_fermante, 2);
			$pos_fermante += 13;
		}
		$pos_courante = $pos_fermante + 2;
	}

	return $texte;
}
