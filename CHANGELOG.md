# Changelog

## 2.2.0 2024-09-30

### Added

- #11 Wheels de détection de la balise `<math>` pour anticiper la disparition dans les futures versions de SPIP
