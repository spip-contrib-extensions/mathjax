<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Traiter les balises math dans les champs inline qui ne passent pas par propre
 * pour ne pas perturber le flow normal du traitement de math sur les champs qui passent dans propre
 * on ne fait rien ici si pas de retour ligne dans le texte
 *
 * @param string $texte
 * @return string
 */
function mathjax_post_typo($texte) {

	if (
		strpos($texte, '<math>') !== false
		&& strpos($texte, "\n") === false
	) {
		if (!function_exists('traiter_math')) {
			include_spip('inc/math');
		}

		$texte = traiter_math($texte, 'typomath', true, false);
		if (strpos($texte, 'base64typomath') !== false) {
			$texte = echappe_retour($texte, 'typomath');
		}
	}
	return $texte;
}

/**
 * Pre-traiter les balises math : si elles contiennent des notes il faut propager dans les notes
 * @param string $texte
 * @return string
 */
function mathjax_pre_propre($texte) {

	// test rapide pour eviter de faire plein de choses pour rien
	if (
		($p = strpos($texte, '<math>')) !== false
		&& (strpos($texte, '[[', $p) !== false)
	) {
		include_spip('inc/math');
		if ($collection = mathjax_collecter_balises_math($texte)) {
			$collection = array_reverse($collection);

			foreach ($collection as $c) {
				if (strpos($c['raw'], '[[') !== false) {
					$texte_math = math_propager_dans_les_notes($c['raw']);
					$texte = substr_replace($texte, $texte_math, $c['pos'], $c['length']);
				}
			}
		}
	}
	return $texte;
}

function mathjax_insert_head_css($flux) {
	$config_json = '{}';

	// il est possible de surcharger mathjax-config.json pour avoir sa propre config compliquée de mathjax
	if ($f = find_in_path('js/mathjax-config.json')) {
		$config_json = file_get_contents($f);
		$config_json = trim($config_json);
	}
	include_spip('inc/config');

	$lib_components = 'tex-chtml.js';
	if (defined('_MATHJAX_LIB_COMPONENTS')) {
		$lib_components = _MATHJAX_LIB_COMPONENTS;
	}
	$libjs = "https://cdn.jsdelivr.net/npm/mathjax@3/es5/$lib_components";
	if (
		lire_config('mathjax/mode_dappel', 'cdn') === 'download'
		&& ($src = find_in_path("lib/mathjax/es5/$lib_components"))
	) {
		$libjs = $src;
	}

	$flux .= '<style>'
	  . 'p.spip-math {margin:1em 0;text-align: center}'
	  // DEBUG : surligner les formules
	  // . ".spip-math {background: yellow}"
	  . '</style>';

	// les scripts inline d'init avant les scripts externes
	$flux = "<script>window.MathJaxLib='{$libjs}';window.MathJax={$config_json};</script>" . $flux;


	return $flux;
}

function mathjax_insert_head($flux) {
	$js_mathjax_start = timestamp(find_in_path('js/mathjax-start.js'));
	$flux .= "<script type='text/javascript' src=\"{$js_mathjax_start}\"></script>";
	return $flux;
}

function mathjax_header_prive_css($flux) {
	return mathjax_insert_head_css($flux);
}

function mathjax_header_prive($flux) {
	return mathjax_insert_head($flux);
}
