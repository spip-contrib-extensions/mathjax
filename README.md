# MathJax pour SPIP

Plugin pour prendre en charge les expressions Tex dans une balise `<math>` dans le contenu éditoral de SPIP
à l'aide de la librairie Javascript MathJax

<a href="https://www.mathjax.org/"><img src="https://www.mathjax.org/badge/mj-logo.svg" width="400" /></a>

## Mode d'emploi

Le plugin prend en charge les formules mathématiques au format Tex dans les contenus SPIP.

Par exemple

```
Si on note $dm$ la masse du tronçon et $\ddot{y}$ son accélération alors le principe de la dynamique nous donne une première relation :
$$
d\vec{F} = dm \ddot{\vec{y}}
$$
```

Mais comme le signe `$` qui délimite les formules au format Tex est un signe commun qui peut être présent dans du contenu éditorial
sans indiquer une formule mathématique, il faut encadrer les contenus que l'on veut traiter par une balise `<math>...</math>`

La plupart du temps, si vous n'avez pas de signe `$` autre dans votre texte, vous pouvez encadrer tout le texte


```
<math>
Si on note $dm$ la masse du tronçon et $\ddot{y}$ son accélération alors le principe de la dynamique nous donne une première relation :
$$
d\vec{F} = dm \ddot{\vec{y}}
$$
</math>
```

Et les formules seront ainsi transformées comme suit


<img src="img/mathjax-demo.png" />

Par ailleurs sur chaque formule un menu contextuel disponible au clic droit vous permet d'exporter la formule dans le format Tex ou MathML,
ainsi que d'accéder à d'autres options

<img src="img/mathjax-demo-menu.png" />

### Modèle `<math|f=...>`

Alternativement à la syntaxe historique de SPIP, il est possible aussi d'utiliser un modèle pour insérer des formules inline sour la forme
`<math|f=...votre_formule en latex...>`

Exemple
`
<math|f=t=\frac{d}{V_{A}+V_{B}}=\frac{70}{80+60}=\frac{70}{140}=0,5\ h=30\ minutes→12h\ 30\ min>
`

donne

<img src="img/mathjax-demo-modele.png" />

Cette syntaxe est issue du plugin mathjax_latex, qui devient obsolète par la même occasion,
tous les utilisateurs pouvant utiliser ce plugin MathJax et l'une ou l'autre syntaxe selon leur préférence


# Configuration

Le plugin peut-être configuré pour utiliser la version CDN de mathjax (par défaut) ou une version téléchargée dans le dossier `lib/` de votre site

<img src="img/mathjax-configuration.png" />

## Configuration avancée de MathJax 3

Si vous voulez utiliser la librairie MathJax de façon avancée, au delà de ce qui est prévu par le plugin,
vous pouvez surcharger le fichier `js/mathjax-config.json` en en mettant une copie dans votre dossier `squelettes/`
et le modifier à votre guise pour y ajouter les options que vous souhaitez.


Par ailleurs, le plugin utilise par défaut le composant `tex-chtml` via le fichier `tex-chtml.js` https://docs.mathjax.org/en/latest/web/components/combined.html#tex-chtml
Si votre usage le nécessite vous pouvez utiliser un autre composant, via un define dans votre fichier `mes_options.php`

```
define('_MATHJAX_LIB_COMPONENTS','tex-svg.js');
```

Le plugin chargera alors ce composant à la place de celui par défaut
