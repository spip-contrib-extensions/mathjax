<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Callback pour les <math></math>
 * Fonction préfixée, permettant ainsi qu'en SPIP 4.x
 * qui contient encore une wheel interne à SPIP pour les maths
 * on n'ai pas de conflit.
 *
 *
 * @param string $t
 * @return string
 */
function mathjax_replace_math(string $t) :string {
	if (!function_exists('traiter_math')) {
		include_spip('inc/math');
	}
	$t = traiter_math($t, '', true);
	return $t;
}
