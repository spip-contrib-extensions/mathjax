function mathjaxStart(){
	if (typeof window.MathJaxLib!=="undefined"){
		if (document.querySelector('.spip-math')) {
			var script = document.createElement('script');
			script.src = window.MathJaxLib;
			script.async = true;
			document.head.appendChild(script);
			// on delete, pour n'executer qu'une seule fois le chargement
			delete window.MathJaxLib;
		}
	}
}
// appliquer mathjax sur les formules chargées dans de l'ajax
function mathjaxOnLoad(){
	var node = ((typeof jQuery !== "undefined" && this instanceof jQuery) ? this.get(0) : this);
	if (node.querySelector('.spip-math')){
		if (typeof window.MathJaxLib!=="undefined"){
			mathjaxStart();
		} else {
			MathJax.typeset(node.querySelectorAll('.spip-math'));
		}
	}
}
if (typeof jQuery !== "undefined") {
	jQuery(mathjaxStart);
	onAjaxLoad(mathjaxOnLoad);
} else {
	document.addEventListener('DOMContentLoaded', mathjaxStart, false);
	window.addEventListener("load", mathjaxStart, false);
}
